package com.theosterkamps.hello;


import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MathBotTest {
	
	@Before
	public void setUp() throws Exception {
		
		
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void divideByTwoPositiveNumbers() {
		MathBot math = new MathBot();
		double actual = math.divide(4, 2);
		Assert.assertEquals(2, actual, 0);
	}

	@Test
	public void divideByTwoNegativeNumbers() {
		MathBot math = new MathBot();
		double actual = math.divide(-14, -2);
		Assert.assertEquals(7, actual, 0);
	}
	
	@Test
	public void divideByOneNegativeNumber() {
		MathBot math = new MathBot();
		double actual = math.divide(10, -2);
		Assert.assertEquals(-5, actual, 0);
	}
	
	@Test
	public void divide0ByNumber() {
		MathBot math = new MathBot();
		double actual = math.divide(0, 2);
		Assert.assertEquals(0, actual, 0);
	}
	
	@Test
	public void divideByTwoDecimals() {
		MathBot math = new MathBot();
		double actual = math.divide(10.5, 5.25);
		Assert.assertEquals(2, actual, 0);
	}
	
	@Test
	public void divideBy0() {
		MathBot math = new MathBot();
		Double actual = math.divide(0, 0);
		boolean isNotANumber = actual.isNaN();
		Assert.assertTrue(isNotANumber);
	}
	
}
